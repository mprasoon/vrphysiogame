from django.conf.urls import url
from api.views import *

urlpatterns = [
    url(r'^user_login/$', LoginView.as_view(), name='api_login'),
    url(r'^users/(?P<id>[0-9]+)/', UserDetail.as_view(), name='api_user_detail'),
    url(r'^userdb/$', UserDBCreate.as_view(), name='api_user_db_create'),
    url(r'^getdata/(?P<id>[0-9]+)/', UserDBBodyMovement.as_view(), name='api_user_db_body_movement'),
]

