from rest_framework.views import APIView
from django.contrib.auth.models import User
from comp.models import UserDatabase
from django.contrib.auth import authenticate
from rest_framework.response import Response
import json
import math

class LoginView(APIView):

    def post(self, request, *args, **kwargs):
        username = request.data['username']
        password = request.data['password']
        user = authenticate(username=username, password=password)
        return Response({'id':user.id, 'username': username})

class UserDetail(APIView):

    def get(self, request, *args, **kwargs):
        _id = kwargs['id']
        u = User.objects.get(id=_id)
        return Response({'username': u.username})

class UserDBCreate(APIView):

    def post(self, request, *args, **kwargs):
        u = User.objects.get(id=request.data['user_id'])
        ud = UserDatabase(user=u, phone_reg_name=request.data['phone_reg_name'])
        ud.save()
        return Response({'id':ud.id})

class UserDBBodyMovement(APIView):

    def get(self, request, *args, **kwargs):
        print("get_api_data")
        print("\n\n")
        user_db = UserDatabase.objects.get(id=kwargs['id'])
        return_json_data = json.loads(user_db.json_data)
        print(return_json_data)
        return Response(return_json_data)
