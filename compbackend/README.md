# VR server backend

## Setting up the project

Create a new virtualenv on python
`virtualenv -p python3 venv`
`source venv/bin/activate`
`pip install -r requirements.txt`

## Starting the project

Migrate the database
`python manage.py migrate`

Run the server
`python manage.py runserver`

## Working on the project

Visit `https://localhost:8000/`
Sign up, and then visit the react application. Create a new session and enter the session name on the upcoming link. Start the pose detection, and enable the webcam.

## Exiting

Stop the server
