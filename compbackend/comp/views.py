from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from comp.models import UserDatabase
import json

def redirect_to_signup(request):
    return redirect('/comp/signup/')

def login_view(request):
    if request.user.is_authenticated:
        return HttpResponse('{} Already logged in'.format(request.user))

    if request.method == 'POST':
        loginform = AuthenticationForm(data=request.POST)
        if loginform.is_valid():
            username = loginform.cleaned_data['username']
            password = loginform.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return redirect('/comp/phone_reg/')
    else:
        loginform = AuthenticationForm()
    return render(request, 'comp/login.html', {'loginform': loginform})

@login_required
def logout_view(request):
    logout(request)
    return redirect('/')

def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            raw_password = form.cleaned_data['password1']
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/comp/phone_reg/')
    else:
        form = UserCreationForm()
    return render(request, 'comp/signup.html', {'form': form})

def phone_reg_view(request):
    return render(request, 'comp/phone_reg.html', {})

def validate_cam_view(request):
    if request.method == 'POST':
        rPOST = dict(request.POST)
        phone_reg_name = rPOST['phone_reg_name'][0]
        user_db = UserDatabase.objects.get(phone_reg_name=phone_reg_name)
        print("Entered here")
        return HttpResponse(str(user_db.id))

def start_cam_det(request, user_db_id):
    user_db = UserDatabase.objects.get(id=user_db_id)
    return render(request, 'comp/start_cam_det.html', {})

def pose_data(request, user_db_id):
    user_db = UserDatabase.objects.get(id=user_db_id)
    if request.method == 'POST':
        print("pose_data")
        print("\n\n")
        given_data = dict(request.POST)
        user_db.json_data = json.dumps(given_data)
        user_db.save()
        print(user_db.json_data)
    return HttpResponse(status=200)
