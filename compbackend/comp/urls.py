from django.conf.urls import url
from comp.views import *

urlpatterns = [
    url(r'^login/$', login_view, name='login'),
    url(r'^signup/$', signup_view, name='signup'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^phone_reg/$', phone_reg_view, name='phone_reg'),
    url(r'^validate_cam/$', validate_cam_view, name='validate_cam'),
    url(r'^start_cam_detect/(?P<user_db_id>[0-9]+)/$', start_cam_det, name='start_cam_detect'),
    url(r'^pose_data/(?P<user_db_id>[0-9]+)/', pose_data, name='pose_data')
]
