
let video;
let poseNet;
let poses = [];

var scriptTsStart = (new Date).getTime();

function getCookie(c_name)
{
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
 }

$.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });


function setup() {
  createCanvas(640, 480);
  video = createCapture(VIDEO);
  video.size(width, height);

  poseNet = ml5.poseNet(video, modelReady);
  poseNet.on('pose', function(results) {
    curTs = (new Date).getTime()
    poses = results;
    postBodyData(poses, curTs);
  });
  video.hide();
}

// while (true) {
//   postBodyData({'as':'as'})
// }

cur_url = window.location.href
cur_url_spl = cur_url.split("/")
post_url = "/comp/pose_data/"+cur_url_spl[cur_url_spl.length-2]+"/"
function postBodyData (poses, curTs) {

  if (((curTs - scriptTsStart ) > 200) && poses[0]) {
    let leftEyeX = poses[0]['pose']['leftEye']['x']
    let leftEyeY = poses[0]['pose']['leftEye']['y']
    let leftEyeConf = poses[0]['pose']['leftEye']['confidence']
    let rightEyeX = poses[0]['pose']['rightEye']['x']
    let rightEyeY = poses[0]['pose']['rightEye']['y']
    let rightEyeConf = poses[0]['pose']['rightEye']['confidence']
    let leftSX = poses[0]['pose']['leftShoulder']['x']
    let leftSY = poses[0]['pose']['leftShoulder']['y']
    let leftSConf = poses[0]['pose']['leftShoulder']['confidence']
    let rightSX = poses[0]['pose']['rightShoulder']['x']
    let rightSY = poses[0]['pose']['rightShoulder']['y']
    let rightSConf = poses[0]['pose']['rightShoulder']['confidence']
    let eyenum = rightEyeY - leftEyeY
    let shouldernum = rightSY - leftSY
    let eyeden = Math.sqrt(Math.pow(rightEyeY - leftEyeY,2) + Math.pow(leftEyeX - rightEyeX,2))
    let shoulderden = Math.sqrt(Math.pow(rightSY - leftSY,2) + Math.pow(rightSX - leftSX,2))
    if ((leftEyeConf > 0.7) && (rightEyeConf > 0.7)) {
      document.getElementById('eye_tilt').innerHTML = eyenum/eyeden
    }
    if ((leftSConf > 0.7) && (rightSConf > 0.7)) {
      document.getElementById('shoulder_tilt').innerHTML = shouldernum/shoulderden
    }
    // console.log(poses)
    scriptTsStart = curTs
    document.getElementById("table_div").style.display = "block"
    post_data = {'eyeTilt': eyenum/eyeden,
                 'shoulderTilt': shouldernum/shoulderden}
    $.ajax({
      type: "POST",
      url: post_url,
      data: post_data,
      success: function (response) {
        console.log("Success")
      },
      error: function (err) {
        console.log(err)
        console.log("Error")
      },
      complete: function () {
        console.log("complete")
      }
    })
  }
}

function modelReady() {
  select('#status').html('Model Loaded');
}

function exitHere () {
  window.location.href = "http://localhost:8000/comp/phone_reg/"
}

function stopDetection () {
  poseNet.off()
}
